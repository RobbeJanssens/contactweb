﻿using Contactenlijst.Database;
using Contactenlijst.Entities;
using Contactenlijst.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Contactenlijst.Controllers {
	public class ContactController : Controller {
		private readonly IContactDatabase _contactDatabase = new ContactDatabase( );

		public IActionResult Index( ) {
			List<ContactIndexViewModel> viewModels = new List<ContactIndexViewModel>();

			foreach (Contact contact in _contactDatabase.GetContacts( ))
				viewModels.Add(new ContactIndexViewModel {
					Name = contact.Name,
					LastName = contact.LastName,
					Guid = contact.Guid
				});

			viewModels = viewModels.OrderBy(c => c.LastName).ThenBy(c => c.Name).ToList( );

			return View(viewModels);
		}

		[HttpGet]
		public IActionResult Create( ) {
			ContactCreateViewModel viewModel = new ContactCreateViewModel() { DateOfBirth = DateTime.Now};

			viewModel.PhoneNumbers.Add(new PhoneNumberViewModel( ));
			viewModel.Emails.Add(new EmailViewModel( ));
			viewModel.Addresses.Add(new AddressViewModel( ) { Guid = Guid.NewGuid( ), RemoveRemoveButton = true });

			return View(viewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Create([FromForm] ContactCreateViewModel contactCreateViewModel) {

			// TODO: finish creating a contact

			if (!TryValidateModel(contactCreateViewModel)) {
				return View(contactCreateViewModel);
			}

			_contactDatabase.Insert(new Contact {
				Name = contactCreateViewModel.FirstName,
				LastName = contactCreateViewModel.LastName,
				DateOfBirth = contactCreateViewModel.DateOfBirth,
				Emails = contactCreateViewModel.Emails.Select(x => x.Value).Where(x => !string.IsNullOrWhiteSpace(x)).ToList( ),
				PhoneNumbers = contactCreateViewModel.PhoneNumbers.Select(x => x.Value).Where(x => !string.IsNullOrWhiteSpace(x)).ToList( ),
				Addresses = contactCreateViewModel.Addresses.Select(x => new Address( ) {
					City = x.City,
					ZipCode = x.ZipCode,
					AddressLine1 = x.AddressLine1,
					AddressLine2 = x.AddressLine2,
					Guid = x.Guid
				}).ToList( ),
				Comment = contactCreateViewModel.Comment,
				Guid = Guid.NewGuid( )
			});

			return RedirectToAction(nameof(Index));
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult AddEmailItem([FromForm] ContactCreateViewModel contactCreateViewModel) {
			contactCreateViewModel.Emails.Add(new EmailViewModel( ));
			return PartialView("EmailItem", contactCreateViewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult AddPhoneNumberItem([FromForm] ContactCreateViewModel contactCreateViewModel) {
			contactCreateViewModel.PhoneNumbers.Add(new PhoneNumberViewModel( ));
			return PartialView("PhoneNumberItem", contactCreateViewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult AddAddressItem([FromForm] ContactCreateViewModel contactCreateViewModel) {
			contactCreateViewModel.Addresses.Add(new AddressViewModel( ) { Guid = Guid.NewGuid( ) });
			return PartialView("AdressItem", contactCreateViewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult RemoveAddressItem(Guid id, [FromForm] ContactCreateViewModel contactCreateViewModel) {
			AddressViewModel addressViewModel = contactCreateViewModel.Addresses.FirstOrDefault(x => x.Guid == id);

			if (!(addressViewModel is null)) {
				contactCreateViewModel.Addresses.Remove(addressViewModel);
				return Json(new { success = true, responseText = "" });
			} else {
				return Json(new { success = false, responseText = "Could not find address" });
			}
		}
	}
}
