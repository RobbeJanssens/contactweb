﻿using Contactenlijst.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contactenlijst.Models {
	public class ContactCreateViewModel {
		[Required(AllowEmptyStrings = false, ErrorMessage = "Verplicht")]
		[DisplayName("Voornaam")]
		public string FirstName { get; set; }

		[DisplayName("Achternaam")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "Verplicht")]
		public string LastName { get; set; }

		[DataType(DataType.Date)]
		[DisplayName("Gebroortedatum")]
		public DateTime DateOfBirth { get; set; }

		[ContactViewModelListValidator("Er Moet minstens 1 email adres worden opgegeven.")]
		public List<EmailViewModel> Emails { get; set; }

		[ContactViewModelListValidator("Er Moet minstens 1 telefoonnummer worden opgegeven.")]
		public List<PhoneNumberViewModel> PhoneNumbers { get; set; }

		public List<AddressViewModel> Addresses { get; set; }

		public string Comment { get; set; }

		public Guid Guid { get; set; }

		public ContactCreateViewModel( ) {
			Emails = new List<EmailViewModel>( );
			PhoneNumbers = new List<PhoneNumberViewModel>( );
			Addresses = new List<AddressViewModel>( );
		}
	}
}
