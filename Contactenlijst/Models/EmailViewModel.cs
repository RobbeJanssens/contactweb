﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static Contactenlijst.Attributes.ContactViewModelListValidator;

namespace Contactenlijst.Models {
	public class EmailViewModel : IValueListValidator {
		[EmailAddress]
		[DisplayName("Email adres")]
		public string Value { get; set; }
	}
}
