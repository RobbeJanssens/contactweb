﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contactenlijst.Models {
	public class AddressViewModel {

		[Required(AllowEmptyStrings = false, ErrorMessage = "Verplicht")]
		[DisplayName("Stad")]
		public string City { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Verplicht")]
		[DisplayName("Postcode")]
		public string ZipCode { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Verplicht")]
		[DisplayName("Adress lijn 1")]
		public string AddressLine1 { get; set; }

		[DisplayName("Adress lijn 2")]
		public string AddressLine2 { get; set; }
		public Guid Guid { get; set; }

		public bool RemoveRemoveButton { get; set; }
	}
}
