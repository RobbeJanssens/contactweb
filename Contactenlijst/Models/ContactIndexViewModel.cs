﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contactenlijst.Models {
	public class ContactIndexViewModel {
		public string Name { get; set; }
		public string LastName { get; set; }
		public string FullName => $"{Name} {LastName}";
		public Guid Guid { get; set; }
	}
}
