﻿using Contactenlijst.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contactenlijst.Database {
	public interface IContactDatabase {
		Contact Insert(Contact contact);
		IEnumerable<Contact> GetContacts( );
		Contact GetContact(Guid guid);
		void Delete(Guid guid);
		void Update(Guid guid, Contact contact);
	}

	public class ContactDatabase : IContactDatabase {
		private static readonly List<Contact> _contacts = new List<Contact>();

		public ContactDatabase( ) {
		}

		public Contact GetContact(Guid guid) {
			return _contacts.FirstOrDefault(x => x.Guid == guid);
		}

		public IEnumerable<Contact> GetContacts( ) {
			return _contacts;
		}

		public Contact Insert(Contact contact) {
			contact.Guid = Guid.NewGuid( );
			_contacts.Add(contact);
			return contact;
		}

		public void Delete(Guid guid) {
			Contact contact = _contacts.SingleOrDefault(x => x.Guid == guid);
			if (contact != null) {
				_contacts.Remove(contact);
			}
		}

		public void Update(Guid guid, Contact updatedContact) {
			Contact contact = _contacts.SingleOrDefault(x => x.Guid == guid);
			if (contact != null) {
				contact.Name = updatedContact.Name;
				contact.LastName = updatedContact.LastName;
				contact.Emails = updatedContact.Emails;
				contact.PhoneNumbers = updatedContact.PhoneNumbers;
				contact.Addresses = updatedContact.Addresses;
				contact.DateOfBirth = updatedContact.DateOfBirth;
			}
		}
	}
}
