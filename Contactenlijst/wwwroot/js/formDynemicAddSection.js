﻿function AddListItemListener(idBtn, url, idContainer) {
    $(`#${idBtn}`).on("click", () => {
        $.ajax({
            async: true,
            data: $("#form").serialize(),
            type: "POST",
            url: url,
            success: function (partialView) {
                $(`#${idContainer}`).html(partialView);
            }
        })
    })
}