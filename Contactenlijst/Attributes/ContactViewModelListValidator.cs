﻿using Microsoft.AspNetCore.DataProtection.XmlEncryption;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contactenlijst.Attributes {
	public class ContactViewModelListValidator : ValidationAttribute {
		//
		// Fields
		//
		private readonly int _minElements;

		//
		// Constructors
		//
		public ContactViewModelListValidator(int minElements, string errorMessage = null) : base(errorMessage) {
			_minElements = minElements;
		}

		public ContactViewModelListValidator(string errorMessage = null) : this(1, errorMessage) { }

		//
		// Methodes
		//
		public override bool IsValid(object value) {
			List<IValueListValidator> list = new List<IValueListValidator>();

			foreach (object item in value as dynamic) {
				list.Add((IValueListValidator)item);
			}

			if (list != null) {
				return list.Where(x => !string.IsNullOrWhiteSpace(x.Value)).Count( ) >= _minElements;
			}
			return false;
		}

		//
		// Inerface
		//
		public interface IValueListValidator {
			public string Value { get; set; }
		}
	}
}
