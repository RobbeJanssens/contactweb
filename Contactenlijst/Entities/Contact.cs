﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Contactenlijst.Entities {
	public class Contact {
		public string Name { get; set; }
		public string LastName { get; set; }
		public DateTime DateOfBirth { get; set; }
		public List<string> Emails { get; set; }
		public List<string> PhoneNumbers { get; set; }
		public List<Address> Addresses { get; set; }
		public string Comment { get; set; }
		public Guid Guid { get; set; }
	}
}
